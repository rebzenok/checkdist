from django.db import models
from datetime import datetime
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

class Dist(models.Model):
	rel_name = models.CharField(max_length=30)
	rel_number = models.PositiveIntegerField()
	date_build = models.DateTimeField(default=datetime.now, blank=True)

class Package(models.Model):
	name = models.CharField(max_length=50)
	version = models.CharField(max_length=15)
	vendor = models.CharField(max_length=20)
	description = models.CharField(max_length=200)

class DistPackage(models.Model):
	dist = models.ForeignKey(Dist, on_delete=models.CASCADE)
	pack = models.ForeignKey(Package, on_delete=models.CASCADE)
	buid_ver = models.CharField(max_length=100)

class Vulnerability(models.Model):
	cve_id = models.CharField(max_length=30, unique=True)
	CVSS = models.PositiveIntegerField()
	description = models.CharField(max_length=200)

class PackVuln(models.Model):
	pack = models.ForeignKey(DistPackage, on_delete=models.CASCADE)
	vuln = models.ForeignKey(Vulnerability, on_delete=models.CASCADE)

class Comments(models.Model):
	number = models.PositiveIntegerField()
	body = models.CharField(max_length=300)

	content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
	object_id = models.PositiveIntegerField()
	content_object = GenericForeignKey('content_type', 'object_id')

#class User, Developer